# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import time
import twill
import ConfigParser

def setup():
    CONFIG_FILE = os.getenv('ASEC_TESTS_CONFIGFILE')
    if not CONFIG_FILE:
        print >> sys.stderr, 'E: you should define ASEC_TESTS_CONFIGFILE'
        sys.exit(1)

    if not os.path.exists(CONFIG_FILE):
        print >> sys.stderr, 'E: missing config file'
        sys.exit(1)

    global config
    config = ConfigParser.ConfigParser()
    config.readfp(file(CONFIG_FILE))

    twill.set_output(file('/dev/null', 'w'))

def test_connection():
    '''Check server is up'''
    base_url = config.get('general', 'base_url')
    twill.commands.reset_browser()
    twill.execute_string('''
go %s
''' % base_url)

def test_asec_theme():
    '''Check asec theme is configured'''
    base_url = config.get('general', 'base_url')
    twill.commands.reset_browser()
    twill.execute_string('''
go %s
find /themes/asec/wcs.css
''' % base_url)

