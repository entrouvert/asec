#! /usr/bin/env python
#
# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import time
from optparse import OptionParser
import rfc822

from wcs import publisher
from wcs.users import User
from wcs.qommon.ident.password_accounts import PasswordAccount
from qommon.ident.password import make_password
from qommon import emails

def main():
    parser = OptionParser()
    parser.add_option('--vhost', dest='vhost', help='virtual host (required)')
    parser.add_option('--name', dest='name', help='user name')
    parser.add_option('--email', dest='email', help='user email (required)')
    parser.add_option('--password', dest='password', help='user password')
    parser.add_option('--notification', dest='notification',
                      metavar='FILENAME', help='notification email template')
    parser.add_option('--notify', dest='notify', metavar='EMAIL')
    parser.add_option('--uid', dest='uid')
    parser.add_option('--gid', dest='gid')
    parser.add_option('--extra', metavar='DIR', action='append',
                      dest='extra', default=[])
    parser.add_option('--settings', dest='settings', metavar='FILENAME')
    options, args = parser.parse_args()

    if not options.vhost:
        parser.error('you need to specify a virtual host')
    if not options.email:
        parser.error('you need to specify an email address for the user')

    for directory in options.extra:
        publisher.WcsPublisher.register_extra_dir(directory)
    if options.settings:
        publisher.WcsPublisher.default_configuration_path = options.settings

    pub = publisher.WcsPublisher.create_publisher()
    pub.app_dir = os.path.join(pub.app_dir, options.vhost)

    if not os.path.exists(pub.app_dir):
        os.mkdir(pub.app_dir)

    if pub.initialize_app_dir():
        # TODO: site just got initialized, create an admin user?
        pass
    pub.set_config()

    u = User()
    u.email = options.email
    u.name = options.name
    u.is_asec_admin = True
    u.store()

    p = PasswordAccount(id=options.email)
    p.hashing_algo = 'sha'
    password = options.password
    if not password:
        password = make_password()
    p.set_password(password)
    p.user_id = u.id
    p.store()

    if options.notification:
        # notify of creation (email...)
        fp = file(options.notification)
        message = rfc822.Message(fp)
        mail_subject = message['Subject']
        mail_body = fp.read()
        data = {
            'name': options.name,
            'email': options.email,
            'password': password,
            'url': 'http://%s' % options.vhost,
        }
        emails.ezt_email(mail_subject, mail_body, data,
                         email_rcpt=u.email, fire_and_forget=False,
                         want_html=False)

    if options.notify:
        data = {
            'hostname': options.vhost,
            'username': options.email,
            'email_as_username': True,
            'name': u.name,
            'email': u.email,
        }

        emails.custom_ezt_email('new-registration-admin-notification', data,
            [options.notify], fire_and_forget=False)

    if options.uid:
        os.system('chown -R %s %s' % (options.uid, pub.app_dir))

    if options.gid:
        os.system('chgrp -R %s %s' % (options.gid, pub.app_dir))


if __name__ == '__main__':
    main()

