#! /usr/bin/env python
#
# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import distutils.core
from quixote.ptl.qx_distutils import qx_build_py

def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.xml', '.html', '.js', '.ezt', '.gif']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append( (root.replace(sourcedir, destdir, 1), l) )
        if 'CVS' in dirs:
            dirs.remove('CVS')
        if '.svn' in dirs:
            dirs.remove('.svn')
        if '.git' in dirs:
            dirs.remove('.git')
    return r

distutils.core.setup(
        name = 'wcs-asec',
        version = '0.5',
        licence='AGPLv3 or later',
        maintainer = 'Frederic Peters',
        maintainer_email = 'fpeters@entrouvert.com',
        package_dir = { 'extra': 'extra' },
        packages = ['extra', 'extra.modules'],
        scripts = ['create_asec_user.py', 'check_asec_vhost.py'],
        cmdclass = {'build_py': qx_build_py},
        data_files = [('share/wcs/', ('asec-wcs-settings.xml', 'asec-welcome-email'))] + \
            data_tree('share/wcs/web/', 'data/web/') + \
            data_tree('share/wcs/texts', 'texts') + \
            data_tree('share/wcs/themes/asec', 'themes/asec') + \
            data_tree('share/wcs/themes/asec-green', 'themes/asec-green')
    )
