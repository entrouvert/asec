# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from qommon.tokens import Token

import wcs.fields
import wcs.formdata
import wcs.formdef
import wcs.users

import anonymity

fill_admin_form_orig = wcs.fields.WidgetField.fill_admin_form

def new_fill_admin_form(self, form):
    fill_admin_form_orig(self, form)
    attributes_to_remove = ['in_listing', 'extra_css_class', 'prefill', 'varname']
    for attribute in attributes_to_remove:
        widget = form._names.get(attribute)
        if not widget:
            continue
        del form._names[attribute]
        form.widgets.remove(widget)

wcs.fields.WidgetField.fill_admin_form = new_fill_admin_form

def formdef_get_private(self):
    return self._private

def formdef_set_private(self, value):
    self._private = value
    if self.private_urlname:
        return
    if not self._private:
        return
    token = Token(expiration_delay=None, size=32)
    token.type = 'private-url'
    token.formdef_id = self.id
    token.store()
    self.private_urlname = token.id

wcs.formdef.FormDef._private = False
wcs.formdef.FormDef.private_urlname = None
wcs.formdef.FormDef.private = property(formdef_get_private, formdef_set_private)
wcs.formdef.FormDef.access_as_password = False
wcs.users.User.is_asec_admin = False
wcs.users.User._hashed_indexes = ['roles']

wcs.formdata.FormData._hashed_indexes = []
