# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

try:
    from hashlib import sha1 as sha
except ImportError:
    from sha import sha

from qommon.storage import StorableObject
from qommon.ident.password import make_password

class ParticipantToken(StorableObject):
    _names = 'participant-tokens'

    token = None

    def __init__(self, formdef_id, user_id):
        StorableObject.__init__(self, '%s-%s' % (formdef_id, user_id))

    def generate_token(self):
        token = make_password(10, 10)
        token = sha(token).hexdigest().upper()[:12]
        self.token = sha(token).hexdigest()
        return token

    def check(self, token):
        return sha(token.upper().replace('-', '')).hexdigest() == self.token

    def get_user_id(self):
        return self.id.split('-', 1)[-1]
    user_id = property(get_user_id)

