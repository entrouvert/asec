# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime

from quixote import get_request, get_publisher
from quixote.errors import AccessError

from qommon import template

from wcs.fields import WidgetField
from wcs.formdef import FormDef

class QuotaExceeded(AccessError):
    def render(self):
        return template.error_page(_('Quota Exceeded'),
               continue_to = (get_publisher().get_root_url(), _('the homepage')))

class NotAvailableFeature(AccessError):
    def render(self):
        return template.error_page(_('This feature is not available.'),
               continue_to = (get_publisher().get_root_url(), _('the homepage')))


def get_max_number_of_forms():
    t = get_request().get_header('x-asec-quota-forms')
    if not t:
        return None
    return int(t)

def may_add_a_new_form():
    max_forms = get_max_number_of_forms()
    if not max_forms:
        return True
    return (FormDef.count() < max_forms)


def get_max_number_of_fields():
    t = get_request().get_header('x-asec-quota-fields')
    if not t:
        return None
    return int(t)

def may_add_a_new_field(formdef):
    max_fields = get_max_number_of_fields()
    if not max_fields:
        return True
    fields_count = 0
    for field in formdef.fields:
        # only count data fields, not presentation fields (pages, titles, etc.)
        if isinstance(field, WidgetField):
            fields_count += 1
    return (fields_count < max_fields)

def get_max_number_of_answers():
    t = get_request().get_header('x-asec-quota-answers')
    if not t:
        return None
    return int(t)

def may_add_a_new_answer(formdef):
    max_answers = get_max_number_of_answers()
    if not max_answers:
        return True
    answers_count = formdef.data_class().count()
    return (answers_count < max_answers)

def get_boolean_quota(s):
    v = get_request().get_header(s)
    if v and v.lower() == 'true':
        return True
    return False

def can_logo():
    return get_boolean_quota('x-asec-can-logo')

def can_theme():
    return get_boolean_quota('x-asec-can-theme')

def can_mail():
    return get_boolean_quota('x-asec-can-mail')

def is_locked():
    return get_boolean_quota('x-asec-locked')

def is_expired():
    t = get_request().get_header('x-asec-expiration-date')
    if not t:
        return False
    try:
        expiration_date = datetime.strptime(t, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        return False
    return expiration_date < datetime.now()
