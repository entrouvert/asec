# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os

from quixote import get_publisher, get_request, get_response, redirect
from quixote.directory import Directory
from quixote.util import StaticDirectory
from quixote.html import TemplateIO, htmltext

from qommon.admin.texts import TextsDirectory
from qommon import template
from qommon import errors
from qommon.tokens import Token

import wcs.root
import wcs.forms
import wcs.admin.root
from wcs.formdef import FormDef

import forms
import backoffice
import boconfig
import quota


class PrivateDirectory(Directory):
    _q_exports = ['']

    def _q_index(self):
        return redirect('..')

    def _q_lookup(self, component):
        try:
            token = Token.get(component)
        except KeyError:
            raise errors.TraversalError()
        if token.type != 'private-url':
            raise errors.TraversalError()

        try:
            formdef = FormDef.get(token.formdef_id)
        except KeyError:
            raise errors.TraversalError()
        if formdef.disabled:
            raise errors.TraversalError()
        if not formdef.private:
            raise errors.TraversalError()

        if not quota.may_add_a_new_answer(formdef):
            raise quota.QuotaExceeded()
        return forms.FormPage(formdef=formdef)


class RootDirectory(wcs.root.RootDirectory):
    _q_exports = ['', 'admin', 'backoffice', 'login', 'logout', 'token',
                  'ident', 'register', 'afterjobs', 'themes', 'p',
                  ('robots.txt', 'robots'), 'pages']
    backoffice = backoffice.RootDirectory()
    admin = wcs.admin.root.RootDirectory()
    p = PrivateDirectory()

    def _q_traverse(self, path):
        response = get_response()
        if not hasattr(response, 'filter'):
            response.filter = {}
        if not hasattr(response, 'breadcrumb'):
            response.breadcrumb = [ ('', _('Home')) ]
        response.filter['logo_url'] = boconfig.get_logo_url()
        if quota.is_locked() and (len(path) == 0 or not path[0] in ['themes', 'robots.txt']):
            return TextsDirectory.get_html_text('asec-locked-site')
        return Directory._q_traverse(self, path)

    def _q_index(self):
        template.html_top()

        formdefs = FormDef.select(order_by='name', ignore_errors=True)

        if quota.is_expired():
            return self.index_expired()

        r = TemplateIO(html=True)
        if len(formdefs) == 0:
            r += TextsDirectory.get_html_text('asec-welcome-empty-site')
        else:
            if get_request().user:
                r += TextsDirectory.get_html_text('welcome-logged')
            else:
                r += TextsDirectory.get_html_text('welcome-unlogged')

            r += htmltext('<ul>')
            for formdef in formdefs:
                if formdef.disabled:
                    continue
                if formdef.private:
                    continue
                r += htmltext('<li><a href="%s/">%s</a></li>') % (formdef.url_name, formdef.name)
            r += htmltext('</ul>')
        return r.getvalue()

    def index_expired(self):
        template.html_top()
        return TextsDirectory.get_html_text('asec-expired-site-homepage')

    def _q_lookup(self, component):
        if component in ('css','images'):
            return StaticDirectory(
                            os.path.join(get_publisher().data_dir, 'web', component),
                            follow_symlinks=True)
        if component == 'qo':
            dirname = os.path.join(get_publisher().data_dir, 'qommon')
            return StaticDirectory(dirname, follow_symlinks=True)

        try:
            f = FormDef.get_by_urlname(component)
        except KeyError:
            raise errors.TraversalError()
        if f.private is True:
            raise errors.TraversalError()
        if not quota.may_add_a_new_answer(f):
            raise quota.QuotaExceeded()
        get_response().breadcrumb.append(('%s/' % f.url_name, f.name))
        return forms.FormPage(formdef=f)

    def robots(self):
        response = get_response()
        response.set_content_type('text/plain')
        return 'User-Agent: *\nDisallow: /p/\n'

from qommon.publisher import get_publisher_class
get_publisher_class().root_directory_class = RootDirectory

TextsDirectory.register('asec-welcome-empty-site',
        N_('Text displayed on the homepage of an empty site'),
        default=N_('''<p>
Welcome on your new site. It is currently empty, you should go to the
<a href="backoffice/">backoffice</a> to create some questionnaires.
</p>
'''))

TextsDirectory.register('asec-locked-site',
        N_('Text displayed when the site is locked'),
        default=N_('''<p>
This site has been locked.
</p>
'''))

TextsDirectory.register('asec-expired-site-homepage',
        N_('Text displayed on the homepage when the site has expired'),
        default=N_('''<p>
This site has expired.
</p>
'''))
