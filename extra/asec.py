# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from qommon.publisher import get_publisher_class

import modules.backoffice
import modules.root

get_publisher_class().register_translation_domain('wcs-asec')
get_publisher_class().default_configuration_path = 'asec-wcs-settings.xml'
get_publisher_class().qommon_admin_css = '../css/admin.css'
get_publisher_class().backoffice_directory_class = modules.backoffice.RootDirectory

