#! /usr/bin/env python
#
# w.c.s. (asec) - w.c.s. extension for poll & survey service
# Copyright (C) 2010-2011  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import time
from optparse import OptionParser
import rfc822
import pwd
import grp

sys.path.append('/home/fred/src/eo/wcs')

from wcs import publisher

def main():
    parser = OptionParser()
    parser.add_option('--vhost', dest='vhost', help='virtual host (required)')
    parser.add_option('--uid', dest='uid')
    parser.add_option('--gid', dest='gid')
    options, args = parser.parse_args()

    if not options.vhost:
        parser.error('you need to specify a virtual host')

    pub = publisher.WcsPublisher.create_publisher()
    pub.app_dir = os.path.join(pub.app_dir, options.vhost)

    if not os.path.exists(pub.app_dir):
        sys.exit(2)

    uid, gid = tuple(os.stat(pub.app_dir)[4:6])
    if options.uid:
        if not pwd.getpwnam(options.uid).pw_uid == uid:
            sys.exit(3)

    if options.gid:
        if not grp.getgrnam(options.gid).gr_gid == gid:
            sys.exit(4)

    sys.exit(0)

if __name__ == '__main__':
    main()

